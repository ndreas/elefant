create table documents (
    id bigserial primary key not null

    , created_at timestamp with time zone not null default now()
    , updated_at timestamp with time zone not null default now()

    , metadata jsonb not null default '{}'::jsonb
    , html text not null default ''
    , screenshot text not null default ''
);

select diesel_manage_updated_at('documents');
