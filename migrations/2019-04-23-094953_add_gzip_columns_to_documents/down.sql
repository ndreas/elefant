alter table documents
    drop column html,
    drop column screenshot;

alter table documents
    rename column html_txt to html;
alter table documents
    rename column screenshot_txt to screenshot;
