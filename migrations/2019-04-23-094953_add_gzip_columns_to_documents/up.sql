alter table documents
    rename column html to html_txt;
alter table documents
    rename column screenshot to screenshot_txt;

alter table documents
    add column html bytea not null default ''::bytea,
    add column screenshot bytea not null default ''::bytea;
