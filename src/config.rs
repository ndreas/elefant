use std::convert::Into;
use std::path::{Path, PathBuf};

use failure::Fallible;
use serde_derive::*;

#[derive(Debug, Deserialize)]
pub struct Config {
    pub log: LogConfig,
    pub http: HttpConfig,
    pub db: DbConfig,
}

#[derive(Debug, Deserialize)]
pub struct LogConfig {
    pub level: String,
    pub file: Option<PathBuf>,
}

#[derive(Debug, Deserialize)]
pub struct HttpConfig {
    pub listen: String,
}

#[derive(Debug, Deserialize)]
pub struct DbConfig {
    pub url: String,
}

impl Config {
    pub fn new() -> Fallible<Self> {
        let mut cfg = config::Config::new();

        cfg.set_default("log.level", "warn")?;
        cfg.set_default("log.file", None::<Option<String>>)?;
        cfg.set_default("http.listen", "127.0.0.1:3000")?;
        cfg.set_default(
            "db.url",
            "postgres://elefant_dev:elefant_dev@127.0.0.1:5432/elefant_dev",
        )?;

        if let Some(f) = [
            "elefant.toml",
            "/usr/local/etc/elefant.toml",
            "/etc/elefant.toml",
        ]
        .iter()
        .find(|p| Path::new(p).exists())
        {
            cfg.merge(config::File::with_name(f))?;
        }

        cfg.merge(config::Environment::with_prefix("ELEFANT"))?;

        cfg.try_into().map_err(Into::into)
    }
}
