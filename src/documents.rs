use std::convert::TryInto;

use actix_web::{web, Error, HttpResponse};
use diesel::prelude::*;
use failure::Fallible;
use futures::Future;
use log::*;
use serde::{Deserialize, Serialize};

use crate::{Connection, Pool};

mod add;

#[derive(Debug, Serialize, Deserialize)]
pub struct Document {
    pub id: i64,
    pub metadata: serde_json::Value,
}

impl Document {
    pub fn save(conn: &Connection, doc: add::Document) -> Fallible<Document> {
        use crate::schema::documents::dsl::*;

        let new_id: i64 = diesel::insert_into(documents)
            .values(&doc)
            .returning(id)
            .get_result(conn)?;

        Ok(Document {
            id: new_id,
            metadata: doc.metadata,
        })
    }
}

pub fn add_handler(
    req: web::Json<add::Body>,
    pool: web::Data<Pool>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    let body = req.0;
    debug!("{:#?}", body.metadata);

    web::block(move || Document::save(&pool.get()?, body.try_into()?)).then(|res| match res {
        Ok(doc) => Ok(HttpResponse::Ok().json(doc)),
        Err(err) => {
            warn!("Failed to save document: {}", err);
            Ok(HttpResponse::InternalServerError().into())
        }
    })
}

pub fn show_handler(
    document_id: web::Path<i64>,
    pool: web::Data<Pool>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || -> Fallible<Option<Vec<u8>>> {
        use crate::schema::documents::dsl::*;
        Ok(documents
            .find(document_id.into_inner())
            .select(html)
            .get_result(&pool.get()?)
            .optional()?)
    })
    .then(|res| match res {
        Ok(Some(doc)) => Ok(HttpResponse::Ok()
            .set_header("Content-Encoding", "gzip")
            .body(doc)),
        Ok(None) => Ok(HttpResponse::NotFound().finish()),
        Err(err) => {
            warn!("Failed to show document: {}", err);
            Ok(HttpResponse::InternalServerError().into())
        }
    })
}

pub fn screenshot_handler(
    document_id: web::Path<i64>,
    pool: web::Data<Pool>,
) -> impl Future<Item = HttpResponse, Error = Error> {
    web::block(move || -> Fallible<Option<Vec<u8>>> {
        use crate::schema::documents::dsl::*;
        Ok(documents
            .find(document_id.into_inner())
            .select(screenshot)
            .get_result(&pool.get()?)
            .optional()?)
    })
    .then(|res| match res {
        Ok(Some(doc)) => Ok(HttpResponse::Ok()
            .set_header("Content-Type", "image/png")
            .set_header("Content-Encoding", "gzip")
            .body(doc)),
        Ok(None) => Ok(HttpResponse::NotFound().finish()),
        Err(err) => {
            warn!("Failed to show document: {}", err);
            Ok(HttpResponse::InternalServerError().into())
        }
    })
}
