table! {
    documents (id) {
        id -> Int8,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        metadata -> Jsonb,
        html -> Bytea,
        screenshot -> Bytea,
    }
}
