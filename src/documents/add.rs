use std::convert::TryFrom;

use deflate::deflate_bytes_gzip;
use diesel::Insertable;
use failure::Error;
use serde::{Deserialize, Serialize};

use crate::schema::documents;

#[derive(Debug, Deserialize, Serialize)]
pub struct Body {
    pub metadata: serde_json::Value,
    pub html: String,
    pub screenshot: String,
}

#[derive(Debug, Insertable)]
#[table_name = "documents"]
pub struct Document {
    pub metadata: serde_json::Value,
    pub html: Vec<u8>,
    pub screenshot: Vec<u8>,
}

impl TryFrom<Body> for Document {
    type Error = Error;

    fn try_from(body: Body) -> Result<Self, Self::Error> {
        failure::ensure!(
            body.screenshot.starts_with("data:image/png;base64,"),
            "Invalid screenshot"
        );

        let screenshot = base64::decode(&body.screenshot[22..])?;
        Ok(Document {
            metadata: body.metadata,
            html: deflate_bytes_gzip(body.html.as_bytes()),
            screenshot: deflate_bytes_gzip(&screenshot),
        })
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use serde_json::Value;
    use std::convert::TryInto;

    fn build_body<T: Into<String>>(html: T, screenshot: T) -> Body {
        Body {
            metadata: Value::Null,
            html: html.into(),
            screenshot: screenshot.into(),
        }
    }

    #[test]
    fn test_body_into_document() {
        let doc: Document = build_body("html", "data:image/png;base64,c2NyZWVuc2hvdA==")
            .try_into()
            .unwrap();
        assert_eq!(doc.metadata, Value::Null);
        assert_eq!(doc.html, deflate_bytes_gzip(b"html"));
        assert_eq!(doc.screenshot, deflate_bytes_gzip(b"screenshot"));
    }

    #[test]
    fn test_body_into_document_errors() {
        type R = Result<Document, failure::Error>;

        let invalid: R = build_body("html", "invalid").try_into();
        assert!(invalid.is_err());

        let wrong_mimetype: R =
            build_body("html", "data:image/jpg;base64,c2NyZWVuc2hvdA==").try_into();
        assert!(wrong_mimetype.is_err());
    }
}
