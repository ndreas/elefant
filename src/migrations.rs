use failure::Fallible;
use log::*;

embed_migrations!();

#[cfg(not(debug_assertions))]
pub fn run(url: &str) -> Fallible<()> {
    use diesel::prelude::*;

    info!("Running migrations");
    debug!("{}", url);

    let conn = PgConnection::establish(url)?;
    embedded_migrations::run(&conn)?;

    Ok(())
}

#[cfg(debug_assertions)]
pub fn run(_url: &str) -> Fallible<()> {
    info!("Skipping migrations in debug mode");
    Ok(())
}
