#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_migrations;

use ::log::*;
use actix_web::{middleware, web, App, HttpServer};
use diesel::prelude::*;
use diesel::r2d2::{self, ConnectionManager};
use failure::Fallible;

mod config;
mod documents;
mod handlers;
mod log;
mod migrations;
mod schema;

use handlers::*;

pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;
pub type Connection = r2d2::PooledConnection<ConnectionManager<PgConnection>>;

const MAX_DOCUMENT_SIZE: usize = 100 * 1024 * 1024;

fn main() -> Fallible<()> {
    let cfg = config::Config::new()?;
    log::init(&cfg.log.level, cfg.log.file)?;
    info!("Elefant");

    migrations::run(&cfg.db.url)?;

    let manager = ConnectionManager::<PgConnection>::new(cfg.db.url);
    let pool = r2d2::Pool::builder().build(manager)?;

    HttpServer::new(move || {
        App::new()
            .data(pool.clone())
            .wrap(middleware::Logger::default())
            .service(
                web::scope("/documents")
                    .service(
                        web::resource("/").route(
                            web::post()
                                .data(web::JsonConfig::default().limit(MAX_DOCUMENT_SIZE))
                                .to_async(documents::add_handler),
                        ),
                    )
                    .service(
                        web::resource("/{id}").route(web::get().to_async(documents::show_handler)),
                    )
                    .service(
                        web::resource("/{id}/screenshot")
                            .route(web::get().to_async(documents::screenshot_handler)),
                    ),
            )
            .service(web::resource("/").route(web::get().to(index_handler)))
    })
    .bind(&cfg.http.listen)?
    .run()?;

    Ok(())
}
