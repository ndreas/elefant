#!/bin/bash
set -e

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
  create user elefant_dev with password 'elefant_dev';
  alter user elefant_dev superuser;
  create database elefant_dev;

  create user elefant_test with password 'elefant_test';
  alter user elefant_test superuser;
  create database elefant_test;
EOSQL
